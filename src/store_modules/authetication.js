import config from '@/config/custom.js'
import axios from 'axios'
import apiCalls from '@/apiCalls'

// Initial State
const state = {
  username: '',
  token: ''
}

// mutations
const mutations = {
  setUsername (state, name) {
    state.username = name
  },

  setToken (state, accessToken) {
    state.token = accessToken
  }
}

// actions
const actions = {
  login (context, value) {
    return apiCalls.login(value).then(response => {
        context.commit('setUsername', value.username)
        context.commit('setToken', response.token)
      })
  },

  logout (context) {
     return apiCalls.logout({ 'token': context.state.token }).finally(() => {
        context.commit('setUsername', '')
        context.commit('setToken', '')
      })
  }
}


export default {
  state,
  actions,
  mutations
}
