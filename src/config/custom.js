'use strict'

module.exports = {
  getBaseUrl: getBaseUrl,
  getHeader: getHeader
}

function getBaseUrl () {
  return 'http://localhost:10010'
}


function getHeader () {
	if (localStorage.vuex !== undefined && JSON.parse(localStorage.vuex) !== undefined && JSON.parse(localStorage.vuex).authentication !== undefined
		&& JSON.parse(localStorage.vuex).authentication.token) {
		return {
			Authorization: 'Bearer ' + JSON.parse(localStorage.vuex).authentication.token
		}
	} else {
		return ''
	}
}
