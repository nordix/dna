import Vue from 'vue'
import Router from 'vue-router'
import login from '@/components/login.vue'
import subscriptions from '@/components/subscriptions.vue'

Vue.use(Router)

export default new Router({
  routes: [
    { path: '/', name: 'root', redirect: 'subscriptions' },
    { path: '/login', name: 'Login', component: login },
    { path: '/subscriptions', name: 'subscriptions', component: subscriptions }
  ]
})
