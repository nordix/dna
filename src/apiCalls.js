import axios from 'axios'
import config from '@/config/custom.js'
import store from './store'
import router from '@/router'
import _ from 'lodash'

export default {
  getHeader () {
    let authHeader = config.getHeader()
    let header = {
      headers: {
        'Content-Type': 'application/json'
      }
    }
    _.extend(header.headers, authHeader)
    return header
  },

  login(value) {
    const baseUrl = config.getBaseUrl()
    var url = baseUrl + `/auth`

    return axios.post(url, value)
      .then(response => (response.data))
      .catch(error => {
        this.errorHandler(error)
      })
  },

  logout(token) {
    const baseUrl = config.getBaseUrl()
    var url = baseUrl + `/revokeauth`

    return axios.post(url, token)
      .then(response => (response.data))
      .catch(error => {
        this.errorHandler(error)
      })
  },

  // Subsciption related calls
  getAllMobileSubs () {
    const baseUrl = config.getBaseUrl()
    var url = baseUrl + `/mobilesubs`

    return axios.get(url, this.getHeader())
      .then(response => (response.data))
      .catch(error => {
        this.errorHandler(error)
      })
  },

  getMobileSub (subID) {
    const baseUrl = config.getBaseUrl()
    var url = baseUrl + `/mobilesubs/${subID}`

    return axios.get(url, this.getHeader())
      .then(success => {
        return success.data
      })
      .catch(error => {
        this.errorHandler(error)
      })
  },

  getAllBroadbandSubs () {
    const baseUrl = config.getBaseUrl()
    var url = baseUrl + `/broadbandsubs`

    return axios.get(url, this.getHeader())
      .then(response => (response.data))
      .catch(error => {
        this.errorHandler(error)
      })
  },

  getBroadbandSub (subID) {
    const baseUrl = config.getBaseUrl()
    var url = baseUrl + `/broadbandsubs/${subID}`

    return axios.get(url, this.getHeader())
      .then(success => {
        return success.data
      })
      .catch(error => {
        this.errorHandler(error)
      })
  },

  // Error Handler
  errorHandler (error) {
    console.log('error')
    console.log(JSON.stringify(error))
    if (error.response && error.response.status === 403) {
      store.dispatch('logout')
      router.push('/login')
    }
    throw error
  }
}