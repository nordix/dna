import Vue from 'vue'
import 'babel-polyfill'
import App from './App.vue'
import Vuetify from 'vuetify'
import router from './router'
import store from './store'
import Notifications from 'vue-notification'

import 'vuetify/dist/vuetify.min.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'

Vue.use(Notifications)
Vue.use(Vuetify, {
  theme: {
    primary: '#FF007D',
    secondary: '#43A047',
    accent: '#FF4081',
    error: '#f44336',
    warning: '#FFA726',
    info: '#2196f3',
    success: '#4caf50'
  }
})

new Vue({
  router,
  store,
  el: '#app',
  render: h => h(App)
}).$mount('#app')